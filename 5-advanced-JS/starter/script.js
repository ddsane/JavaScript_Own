//Function Constructor
/*
var akash = {
    name: 'Akash',
    DOB: 1992,
    job: 'Design Engineer'


};

var Person = function(name, DOB, job){
    this.name = name;
    this.DOB = DOB;
    this.job = job;

}

Person.prototype.calculateAge = function(){
    console.log(2018 - this.DOB);
}

Person.prototype.currentCompany = 'FrontLine Electronics Ltd';


var mahadev =  new Person('Mahadev', 1992, 'Store Engineer');
console.log(mahadev.DOB);
console.log(mahadev.name);
console.log(mahadev.job);
console.log(mahadev.currentCompany);
mahadev.calculateAge();


var priya = new Person('Priya', 1991, 'Pre-Engineering');
priya.calculateAge();
console.log(priya.currentCompany);


var akash = new Person('sonali', 1989, 'DND1');
akash.calculateAge();
console.log(akash.currentCompany);


//var production = new Person('Sachin', 1991, 'Production Engineer');
//console.log(production.DOB);
//console.log(production.name);
//console.log(production.job);

*/



//Object.Create

/*
var androidProto = {
    rule: function() {
        console.log(6.0 - this.currentVersion);
    }
}


var MarshMellow = Object.create(androidProto);
MarshMellow.name = 'MarshMellow 6.0';
MarshMellow.ui = 'Easy';

var Nougat = Object.create(androidProto);
Nougat.name = 'Nougat 7.0';
Nougat.ui = 'Most Easy';


var Oero = Object.create(androidProto,
{
    name: { value: 'MostGoodOreo'},
    ui: {value: 'SliderAdded'}
})
*/

//Primitive & Objects

/*
var a = 10;
var b = a;
a = 20;
console.log("Value of a" +a+ "Value of B" +b);



var obj1 = {
    name: 'Object name',
    age: 22
}
//console.log(obj1.age);

var obj2 = obj1;
obj1.age = 28;

console.log(obj1.age);
console.log(obj2.age);

var age = 54;
var obj = {
    name: 'Jonas',
    city: 'lisbon'
};

function change (a, b){
    a = 30;
    b.city = 'SanFrancisco';
}

change(age, obj);
console.log(age);
console.log(obj.city);

*/


/*
//Passing function as Arguments


var arrYears = [1991, 1996, 1998, 2002, 2017];

function arrayCalulator(arr, fn){
    var arrRes = [];
    for (var i = 0; i < arr.length; i++){
        arrRes.push(fn([arr[i]]));
    }
    return arrRes;
}

function calculateAge(el){
    return 2019 - el;
}

function isFullAges(age){
    if (calculateAge(age) > 18 ){
        console.log("The Age is Full");
    }else{
        console.log("The Age is Less");
    }

}

var fullAge = arrayCalulator(arrYears, isFullAges);
//console.log("The Full Age is "+fullAge);
//
//var ages = arrayCalulator(arrYears, calculateAge);
//console.log("The Ages Are" +ages);



//var name = ['Charle', 'Matt', 'Larry', 'Sergy'];
//
//function lengthCalulator(array, fn){
//    var arrRes = [];
//    for (var i = 0; i<arr.length; i++){
//        arrRes.push(fn(arr[i]));
//    }
//
//    return arrRes;
//}
 */

//fIRST CLASS FUNCTIONS: FUNCTIONS RETURNING FUNCTIONS
//
//function interviewquestion(job) {
//    if (job === 'Designer') {
//        return function(name) { ///ANNonymous Function
//                console.log(name + ' , can you exlpain what is UX Design');
//        }
//    }else if (job === 'Teacher') {
//        return function(name) {
//            console.log(name + ' , How Much Experiance did you have');
//        }
//    }else {
//        return function(name) {
//            console.log ("Hello are you there for job");
//    }
//     }
//}
//
////
////var askDesignerQuestion = interviewquestion('Designer');
////askDesignerQuestion('Akash');
////
////
////var askTeacherQuestion = interviewquestion('Teacher');
////askTeacherQuestion('Mahadev');
////
//
//
//interviewquestion('Teacher')('Mark');


//Immediately Invoked Functions Expression
//
//function gameScore() {
//  var score = Math.random() * 10;
//  console.log(score >= 5);
//}

//gameScore();


//IIFE
//( function () {
//    var score = Math.random() * 10;
//    console.log(score >= 5);
//})();


//( function (newScore) {
//    //newScore = 1;
//    var score = Math.random() * 10;
//    console.log("Before Subtraction" +' '+ (newScore - score));
//    console.log(score - newScore <=  5 ? "Equals to 5" : "Not Equals to 5");
//})(5);


//CLOSURES
//
//function retirementAge (retireAge) {
//    var a = ' Years Left for Retirement';
//    return function (yearofBirth) {
//        var age = 2018 - yearofBirth;
//        console.log((retireAge - age) + a);
//    }
//}
//
//var retirementIndia =  retirementAge(58);
//retirementIndia(1990);
//

// function interviewQuestion (job) {
//     if (job === 'Designer') {
//         return function (empName) {
//             console.log( empName + ' ' + "Can you tell me About UI and UX");
//         }
//     }else if (job == 'Programmer') {
//             return function (empName) {
//                 console.log( empName + ' ' + "On Which Technology did you worked");
//             }
//     }else if (job == 'Teacher') {
//                 return function (empName) {
//                 console.log( empName + ' ' + "How Much Experiance did You have");
//             }
//         }else {
//             return function (empName){
//                 console.log( empName + ' ' + "You are Jobless");
//             }
//
//         }
//
//   }
//
// var askInterViewQuestion = interviewQuestion('Designer');
// askInterViewQuestion('Akash');
//
// var askInterViewQuestion = interviewQuestion('Programmer');
// askInterViewQuestion('Sunil Sir');
//
// var askInterViewQuestion = interviewQuestion('Teacher');
// askInterViewQuestion('Devendra');
//
// var askInterViewQuestion = interviewQuestion('Nothing');
// askInterViewQuestion('Swapnil');




// var jhon = {
//   name: "Jhon",
//   age: 25,
//   comapny: "FEL",
//   presentationTime: function(style,timeofDay) {
//         if(style === "Formal"){
//           console.log(timeofDay+ "All. I\'m' " +this.name + " How are you all");
//         }else if (style === "Party") {
//           console.log("Hey Guy\'s ' Lets Party for" +timeofDay);
//         }
//   }
// }
//
// jhon.presentationTime('Formal','morning');
// jhon.presentationTime('Party','Evening');
//
//
// var dev = {
//   name: "Devendra",
//   job: "?",
//   comapny: "??"
//   presentationTime:  function(style,timeofDay) {
//         if(style === "Formal"){
//           console.log(timeofDay+ "All. I\'m' " +this.name + " How are you all");
//         }else if (style === "Party") {
//           console.log("Hey Guy\'s ' Lets Party for" +timeofDay);
//         }
//   }
// }
