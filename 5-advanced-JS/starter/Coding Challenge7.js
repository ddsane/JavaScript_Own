//////////////////////////////////Part 1///////////////////////////////////


// // (function () {
//   function Question (question, answer, correct) {
//      this.question= question;
//      this.answer= answer;
//      this.correct= correct;
//   }
//
//   Question.prototype.displayQuestion = function() {
//     //for displaying question
//     console.log(this.question);
//     //for answer in options
//     for (var i = 0; i < this.answer.length; i++) {
//         console.log( i + ': ' + this.answer[i]);
//     }
//   }
//
//   Question.prototype.checkanswer = function(ans) {
//       if (ans === this.correctanswer) {
//           console.log('Correct Answer');
//       }else {
//         console.log('Wrong Answer');
//       }
//   }
//
//   var q1 = new Question
//   (
//         'What is your name',
//         ['Yash','Devendra','Amit','Ajit'],
//         1
//   );
//   var q2 = new Question(
//        'What is Your Age',
//        [21,26,27],
//        1);
//
//   var q3 = new Question(
//       'What u Love to do',
//       ['Coding','Playing','Watching Movies'],
//       0);
//
//
//   var questions = [q1, q2, q3];
//
//   var n = Math.floor(Math.random() * questions.length);
//   questions[n].displayQuestion();
//
//   var answer = parseInt(prompt("Please select the Correct answer"));
//
//   questions[n].checkanswer(answer);
// // })();


/////////////////////////////////Part 2//////////////////////////////////
(function () {
 function Question (question, answer, correct) {
    this.question= question;
    this.answer= answer;
    this.correct= correct;
 }

 Question.prototype.displayQuestion = function() {
   //for displaying question
   console.log(this.question);
   //for answer in options
   for (var i = 0; i < this.answer.length; i++) {
       console.log( i + ': ' + this.answer[i]);
   }
 }

 Question.prototype.checkanswer = function(ans, callback) {
    var sc;
     if (ans === this.correct) {
         console.log('Correct Answer');
         sc = callback(true);

     }else {
       console.log('Wrong Answer');
       sc = callback(false);
     }
       this.displayScore(sc);
 }



 Question.prototype.displayScore = function(score) {
    console.log('Your Current Score is ' + score);
    console.log('-------------------------------');
 };

 var q1 = new Question
 (
       'What is your name',
       ['Yash','Devendra','Amit','Ajit'],
       1
 );
 var q2 = new Question(
      'What is Your Age',
      [21,26,27],
      1);

 var q3 = new Question(
     'What u Love to do',
     ['Coding','Playing','Watching Movies'],
     0);


  function score() {
    var sc=0;
    return function(correct) {
      if (correct) {
          sc++;
      }
      return sc;
    }
  }
  var keepscore = score();

  var questions = [q1, q2, q3];


function nextQuestion() {
  var n = Math.floor(Math.random() * questions.length);
  questions[n].displayQuestion();

  var answer = prompt('Please select the Correct answer');

  if(answer !== 'exit') {
      questions[n].checkanswer(parseInt(answer), keepscore);
      nextQuestion();
    }

  }

    nextQuestion();


  })();
