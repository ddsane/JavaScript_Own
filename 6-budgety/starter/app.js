
//budget controller
 var budgetController = (function() {
    var Expense = function (id, description, value){
      this.id = id;
      this.description = description;
      this.value = value;
    };

    var Income = function (id, description, value){
      this.id = id;
      this.description = description;
      this.value = value;
    };

    var data = {
      allItems: {
        exp: [],
        inc: []
      },
      totals: {
        exp: 0,
        inc: 0
      }

    };


    return {
      addItem: function(type, desc, val) {
        var newItem, ID;

      //Create ID

          ID = data.allItems[type][data.allItems[type].length - 1].id + 1;
            

      //Creating type exp or inspect
        if (type === 'exp') {
            newItem = new Expense(ID, desc, val);
        }else if (type === 'inc') {
          newItem = new Income(ID, desc, val);
        }

        data.allItems[type].push(newItem);
        return newItem;
      },

      testing: function() {
        console.log(data);
      }
    };


 })();



//Handling UI
 var UIController = (function () {

   var DOMStrings = {
     inputType: '.add__type',
     inputDescription: '.add__description',
     inputValue: '.add__value',
     inputAddBtn: '.add__btn'
   };
     return {
         getInput: function() {
           return {
              type:  document.querySelector(DOMStrings.inputType).value,
              description:  document.querySelector(DOMStrings.inputDescription).value,
              value:  document.querySelector(DOMStrings.inputValue).value
           };
         },

         getDomsStrings: function() {
           return DOMStrings;
         }
     };
 })();


//GLOBAL APP controller
var controller = (function(budjetController, UIController) {
  var setupEventListner = function() {
      var DOM = UIController.getDomsStrings();
    document.querySelector(DOM.inputAddBtn).addEventListener('click', controladdItem);
    document.addEventListener('keypress', function(event) {
        if (event.keycode === 13 || event.which === 13) {
            controladdItem();
        }
    });
  };


  var controladdItem = function() {
    //1. Get the input field data
    var input = UIController.getInput();
    // console.log("Input Params", input);
    var newItem = budgetController.addItem(input.type, input.description, input.value);
  };
  return {
     init: function() {
       console.log("App Started");
       setupEventListner();
     }
  };

})(budgetController, UIController);


controller.init();
